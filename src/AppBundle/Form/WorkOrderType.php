<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkOrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt', null, [
                'label' => 'Date',
                'widget' => 'single_text',
                'html5' => false,
                'format' =>  "MM/dd/yyyy",
                'attr' => [
                    'data-format' =>  "MM/dd/yyyy",
                    'class' => 'datepicker'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    '' => '',
                    'Payment' => 'Payment',
                    'Work orders' => 'Work orders'
                ]
            ])
            ->add('description')
            ->add('value', MoneyType::class, [
                'currency' => 'USD',
                'label' => 'Payment'
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\WorkOrder',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_workorder';
    }


}
