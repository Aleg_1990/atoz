<?php

namespace AppBundle\Form;

use FOS\UserBundle\Form\Type\ProfileFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class TechnicianType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // save fields to change order
        $email = $builder->get('email');
//        $username = $builder->get('username');
//        $plainPassword = $builder->get('current_password');

        $appliances = [
            'cooktop'          => 'Cooktop',
            'dishwasher'       => 'Dishwasher',
            'dryer'            => 'Dryer',
            'exhaust_fan'      => 'Exhaust fan/Hood',
            'freezer'          => 'Freezer (free standing)',
            'darbage_disposal' => 'Garbage Disposal',
            'ice_machine'      => 'Ice machine (free standing)',
            'microwave'        => 'Microwave',
            'oven'             => 'Oven',
            'range'            => 'Range',
            'refrigerator'     => 'Refrigerator',
            'stackable_dryer'  => 'Stackable dryer',
            'stackable_washer' => 'Stackable washer',
            'trash_compactor'  => 'Trash Compactor',
            'washer'           => 'Washer',
            'wine_cooler'      => 'Wine cooler',
        ];

        $builder
            ->remove('email')
            ->remove('username')
            ->remove('current_password')

//            ->add($username)
            ->add('firstname')
            ->add('lastname')
            ->add($email)
//            ->add($plainPassword)
            ->add('address')
            ->add('days', ChoiceType::class, [
                'label' => 'Preferred days of work',
                'multiple' => true,
                'attr' => ['class' => 'select2'],
                'choices' => array_flip(['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']),

            ])
            ->add('phone', null, [
                'attr' => [
                    'data-inputmask' => '\'mask\': \'(999) 999-9999\'',
                    'data-mask' => true
                ]
            ])
            ->add('specialize', ChoiceType::class, [
                'label' => 'Select the appliances you specialize in',
                'multiple' => true,
                'attr' => ['class' => 'select2'],
                'choices' => array_flip($appliances),
            ])
            ->add('dont_work', ChoiceType::class, [
                'label' => 'Select the appliances you don\'t work with',
                'multiple' => true,
                'attr' => ['class' => 'select2'],
                'choices' => array_flip($appliances),
            ])
            ->add('years', IntegerType::class, [
                'label' => 'Years in business',
            ])
            ->add('licensed', ChoiceType::class, [
                'label' => 'Are you licensed?',
                'choices' => [
                    ''    => '',
                    'Yes' => 'Yes',
                    'No'  => 'No',
                    'N/A' => 'N/A'
                ]
            ])
            ->add('insurance', ChoiceType::class, [
                'label' => 'Do you carry business insurance',
                'expanded' => true,
                'choices' => [
                    'Yes' => 'Yes',
                    'No'  => 'No'
                ]
            ])
            ->add('insurance_type', ChoiceType::class, [
                'label' => 'What type of insurance you have?',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    'Commercial General Liability' => 'Commercial General Liability',
                    'Commercial Auto Liability' => 'Commercial Auto Liability',
                    'Worker\'s Compensation' => 'Worker\'s Compensation'
                ]
            ])
        ;
    }

    public function getParent()
    {
        return ProfileFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }
}