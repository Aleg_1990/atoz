<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    /**
     * @Route("/admin/dashboard", name="admin_dashboard")
     * @Template()
     */
    public function dashboardAction(Request $request)
    {
        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_technician_list');
        }
        if($this->getUser()->getTechnician()) {
            return $this->redirectToRoute('admin_my_orders');
        }
        return [];
    }

    /**
     * @Route("/admin/my-orders", name="admin_my_orders")
     * @Template()
     */
    public function myOrdersAction(Request $request)
    {

        $orders = $this->getDoctrine()->getRepository('AppBundle:WorkOrder')
            ->findBy(['user' => $this->getUser()]);

        $sum = 0;
        foreach ($orders as $order) {
            if($order->getType() === 'Payment') {
                $sum -= $order->getValue();
            } else {
                $sum += $order->getValue();
            }
        }

        return [
            'orders' => $orders,
            'sum' => $sum
        ];
    }

    /**
     * @Route("/admin/invoices", name="admin_invoices")
     *
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function invoicesAction(Request $request)
    {
        $invoices = (new Finder())
            ->files()
            ->name('*.pdf')
            ->in($this->getParameter('kernel.root_dir').'/../web/files/pdf')
            ->sort(function (\SplFileInfo $a, \SplFileInfo $b) {
                return $b->getMTime() > $a->getMTime();
            })->getIterator();
        $files = [];
        foreach ($invoices as $invoice) {
            $files[] = [
                'name' => (int) $invoice->getFilename(),
                'slug' => dechex((int) $invoice->getFilename()),
                'modified' => $invoice->getMTime()
            ];
        }
        return ['invoices' => $files];
    }
}
