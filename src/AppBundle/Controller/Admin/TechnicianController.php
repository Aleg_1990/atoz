<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use AppBundle\Entity\WorkOrder;
use AppBundle\Form\TechnicianType;
use AppBundle\Form\WorkOrderType;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * @Route("/admin/technician")
 *
 * @Security("has_role('ROLE_ADMIN')")
 *
 * @Template()
 */
class TechnicianController extends Controller
{
    /**
     * @Route("/create", name="admin_technician_create")
     * @Template()
     */
    public function createAction(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();


//            $event = new FormEvent($form, $request);
//            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
            $user
                ->setEnabled('true')
                ->setTechnician(true);
            $userManager->updateUser($user);

//            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('admin_technician_list');
            $this->addFlash('success', 'Technician has been created!');
                $response = new RedirectResponse($url);
//            }

//            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/list", name="admin_technician_list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $technicians = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findBy(['technician' => true], ['enabled' => 'DESC', 'firstname' => 'ASC', 'lastname' => 'ASC']);

        return ['technicians' => $technicians];
    }

    /**
     * @Route("/{id}/edit", name="admin_technician_edit")
     * @Template()
     */
    public function editAction(Request $request, User $technician)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $this->createForm(TechnicianType::class, $technician, ['validation_groups' => ['Profile', 'TechnicianEdit']]);

        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

//            $event = new FormEvent($form, $request);
//            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);
//            var_dump($request->request->get('submit'));die;
            $technician->setEnabled($request->request->get('submit') !== 'disable');

            $userManager->updateUser($technician);

//            if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('admin_technician_list');
            $this->addFlash('success', 'Technician has been updated!');
            $response = new RedirectResponse($url);
//            }

//            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return [
            'form' => $form->createView(),
            'technician' => $technician
        ];
    }

    /**
     * @Route("/{id}/order/list", name="admin_technician_order_list")
     * @Template()
     */
    public function ordersAction(Request $request, User $technician)
    {

        $order = (new WorkOrder())
            ->setUser($technician)
            ->setCreatedAt(new \DateTime());
        $form = $this->createForm(WorkOrderType::class, $order);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            $url = $this->generateUrl('admin_technician_order_list', ['id' => $technician->getId()]);
            $this->addFlash('success', 'Order has been created!');
            return new RedirectResponse($url);
        }

        $orders = $this->getDoctrine()->getRepository('AppBundle:WorkOrder')
            ->findBy(['user' => $technician]);

        $sum = 0;
        foreach ($orders as $order) {
            if($order->getType() === 'Payment') {
                $sum -= $order->getValue();
            } else {
                $sum += $order->getValue();
            }
        }

        return [
            'technician' => $technician,
            'orders' => $orders,
            'form' => $form->createView(),
            'sum' => $sum
        ];
    }

    /**
     * @Route("/order/{id}/edit", name="admin_order_edit")
     * @Template()
     */
    public function editOrderAction(Request $request, WorkOrder $order)
    {
        $form = $this->createForm(WorkOrderType::class, $order);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            $url = $this->generateUrl('admin_technician_order_list', ['id' => $order->getUser()->getId()]);
            $this->addFlash('success', 'Order has been updated!');
            return new RedirectResponse($url);
        }

        return [
            'form' => $form->createView(),
            'order' => $order
        ];
    }

    /**
     * @Route("/order/{id}/delete", name="admin_order_delete")
     */
    public function deleteOrderAction(Request $request, WorkOrder $order)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($order);
        $em->flush();

        $url = $this->generateUrl('admin_technician_order_list', ['id' => $order->getUser()->getId()]);
        $this->addFlash('success', 'Order has been deleted!');
        return new RedirectResponse($url);
    }
}
