<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Route("/book", name="book")
     * @Route("/contact", name="contact")
     * @Route("/thank", name="thank")
     * @Route("/corporate", name="corporate")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/contact-submit", name="contact_submit")
     */
    public function contactAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $message = \Swift_Message::newInstance()
            ->setSubject('[' . $data['type'] . '_form] ' . (isset($data['subject']) ? $data['subject'] : 'A to Z appliance book form'))
            ->setFrom([$data['email']], $data['name'])
            ->setTo(['sales@a-zappliances.com'])
            ->setBody(
                "Name: " . $data['name'] . "\r\n".
                "Email: " . $data['email'] . "\r\n".
                "Phone: " . $data['phone'] . "\r\n".
                ($data['type'] === 'contact' ? "Subject: " . $data['subject'] . "\r\n" : '').
                "Message: " . stripslashes($data['message'])
            );

        return $this->json(['success' => $this->get('mailer')->send($message)]);
    }

    /**
     * @Route("/registered", name="registered")
     * @Template()
     */
    public function registeredAction(Request $request)
    {
        return [];
    }
}
