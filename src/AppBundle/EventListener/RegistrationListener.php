<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Listener responsible to change the registration process
 */
class RegistrationListener implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $templating;

    public function __construct(UrlGeneratorInterface $router, \Swift_Mailer $mailer, TwigEngine $templating)
    {
        $this->router     = $router;
        $this->mailer     = $mailer;
        $this->templating = $templating;
    }
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => [
                ['onRegistrationSuccess', -10],
            ]
        );
    }

    /**
     * @param GetResponseUserEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        /**
         * @var $user User
         */
        $user = $event->getForm()->getData();
        $this->sendEmail($user);
        $user
            ->setTechnician(true)
            ->setEnabled(false)
        ;
        $url = $this->router->generate('registered');
        $response = new RedirectResponse($url);
        $event->setResponse($response);
    }

    /**
     * @param User $user
     */
    private function sendEmail(User $user)
    {
        $choices = [
            'appliances' => [
                'cooktop'          => 'Cooktop',
                'dishwasher'       => 'Dishwasher',
                'dryer'            => 'Dryer',
                'exhaust_fan'      => 'Exhaust fan/Hood',
                'freezer'          => 'Freezer (free standing)',
                'darbage_disposal' => 'Garbage Disposal',
                'ice_machine'      => 'Ice machine (free standing)',
                'microwave'        => 'Microwave',
                'oven'             => 'Oven',
                'range'            => 'Range',
                'refrigerator'     => 'Refrigerator',
                'stackable_dryer'  => 'Stackable dryer',
                'stackable_washer' => 'Stackable washer',
                'trash_compactor'  => 'Trash Compactor',
                'washer'           => 'Washer',
                'wine_cooler'      => 'Wine cooler',
            ],
            'days' => ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        ];

        $message = \Swift_Message::newInstance()
            ->setSubject('NEW TECHNICIAN CANDIDATE APPLICATION')
            ->setFrom('sales@a-zappliances.com')
            ->setCc(['sales@a-zappliances.com'])
            ->setTo(['aleg914@gmail.com', 'contacts@a-zappliances.com'])
            ->setBody(
                $this
                    ->templating
                    ->render('@App/Email/register.html.twig', [
                        'user' => $user,
                        'choices' => $choices
                    ]),
                'text/html'
            );

        return $this->mailer->send($message);
    }
}