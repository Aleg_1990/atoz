<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter first name.", groups={"Registration", "TechnicianEdit"})
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter last name.", groups={"Registration", "TechnicianEdit"})
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter address.", groups={"Registration", "Profile"})
     */
    protected $address;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     *
     * @Assert\NotBlank(message="Please choose days.", groups={"Registration", "TechnicianEdit"})
     */
    protected $days;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter phone.", groups={"Registration", "Profile"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     *
     * @Assert\NotBlank(message="Please choose aplliances.", groups={"Registration", "TechnicianEdit"})
     */
    protected $specialize;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $dont_work;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter number of years.", groups={"Registration", "TechnicianEdit"})
     */
    protected $years;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(groups={"Registration", "TechnicianEdit"})
     */
    protected $licensed;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(groups={"Registration", "Profile"})
     */
    protected $insurance;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $insurance_type;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $technician = false;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * User construct
     */
    public function __construct()
    {
        parent::__construct();
        $this->username = 'initial'; // to prevent validation
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->setCreatedAt(new \DateTime());
    }

    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set days
     *
     * @param array $days
     *
     * @return User
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return array
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set specialize
     *
     * @param array $specialize
     *
     * @return User
     */
    public function setSpecialize($specialize)
    {
        $this->specialize = $specialize;

        return $this;
    }

    /**
     * Get specialize
     *
     * @return array
     */
    public function getSpecialize()
    {
        return $this->specialize;
    }

    /**
     * Set dontWork
     *
     * @param array $dontWork
     *
     * @return User
     */
    public function setDontWork($dontWork)
    {
        $this->dont_work = $dontWork;

        return $this;
    }

    /**
     * Get dontWork
     *
     * @return array
     */
    public function getDontWork()
    {
        return $this->dont_work;
    }

    /**
     * Set years
     *
     * @param integer $years
     *
     * @return User
     */
    public function setYears($years)
    {
        $this->years = $years;

        return $this;
    }

    /**
     * Get years
     *
     * @return integer
     */
    public function getYears()
    {
        return $this->years;
    }

    /**
     * Set licensed
     *
     * @param string $licensed
     *
     * @return User
     */
    public function setLicensed($licensed)
    {
        $this->licensed = $licensed;

        return $this;
    }

    /**
     * Get licensed
     *
     * @return string
     */
    public function getLicensed()
    {
        return $this->licensed;
    }

    /**
     * Set insurance
     *
     * @param string $insurance
     *
     * @return User
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return string
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Set insuranceType
     *
     * @param string $insuranceType
     *
     * @return User
     */
    public function setInsuranceType($insuranceType)
    {
        $this->insurance_type = $insuranceType;

        return $this;
    }

    /**
     * Get insuranceType
     *
     * @return string
     */
    public function getInsuranceType()
    {
        return $this->insurance_type;
    }

    /**
     * Set technician
     *
     * @param boolean $technician
     *
     * @return User
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;

        return $this;
    }

    /**
     * Get technician
     *
     * @return boolean
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
