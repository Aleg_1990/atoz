<?php

namespace InvoiceBundle\Controller;

use InvoiceBundle\Form\InvoiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DefaultController extends Controller
{
    const choices = [
    'appliance' => [
        'cooktop'          => 'Cooktop',
        'dishwasher'       => 'Dishwasher',
        'dryer'            => 'Dryer',
        'exhaust_fan'      => 'Exhaust fan/Hood',
        'freezer'          => 'Freezer (free standing)',
        'darbage_disposal' => 'Garbage Disposal',
        'ice_machine'      => 'Ice machine (free standing)',
        'microwave'        => 'Microwave',
        'oven'             => 'Oven',
        'range'            => 'Range',
        'refrigerator'     => 'Refrigerator',
        'stackable_dryer'  => 'Stackable dryer',
        'stackable_washer' => 'Stackable washer',
        'trash_compactor'  => 'Trash Compactor',
        'washer'           => 'Washer',
        'wine_cooler'      => 'Wine cooler',
        'other'            => 'Other',
    ],
    'brand' => [
        'amana' => 'Amana',
        'asko' => 'ASKO',
        'bertazzoni' => 'Bertazzoni',
        'bosch' => 'BOSCH',
        'dacor' => 'Dacor',
        'electrolux' => 'Electrolux',
        'eurotech' => 'EUROTECH',
        'fisher_paykel' => 'Fisher&Paykel',
        'frigidaire' => 'Frigidaire',
        'gaggenau' => 'Gaggenau',
        'ge' => 'GE',
        'ge_monogram' => 'GE Monogram',
        'haier' => 'Haier',
        'hot_point' => 'Hot Point',
        'jenn_air' => 'Jenn-Air',
        'kenmore' => 'Kenmore',
        'kitchenaid' => 'Kitchenaid',
        'lg' => 'LG',
        'liebherr' => 'LIEBHERR',
        'magic_chef' => 'Magic Chef',
        'maytag' => 'Maytag',
        'miele' => 'Miele',
        'nutone' => 'Nutone',
        'samsung' => 'Samsung',
        'sub_zero' => 'Sub Zero',
        'thermador' => 'Thermador',
        'u_line' => 'U-Line',
        'viking' => 'Viking',
        'whirlpool' => 'Whirlpool',
        'wolf' => 'Wolf',
        'other' => 'Other ',
    ],
    'condition' => [
        'good' => 'Good',
        'fair' => 'Fair',
        'poor' => 'Poor',
    ],
    'color' => [
        'white' => 'White',
        'stainless' => 'Stainless',
        'grey' => 'Grey',
        'wood_panels' => 'Wood panels',
        'blue' => 'Blue',
        'green' => 'Green',
        'red' => 'Red',
        'black' => 'Black',
        'other' => 'Other',
    ],
    'fuel' => [
        'gas'      => 'Natural gas',
        'electric' => 'Electric',
        'dual'     => 'Dual'
    ],
    'region' => [
        'CA' => 'California',
        'DC' => 'District of Columbia',
    ],
    'payment_method' => [
        'credit_card' => 'Credit card',
        'cash'        => 'Cash',
        'check'       => 'Check',
        'not_collected' => 'Not collected'
    ],
    'yesno' => [
        'Yes' => 'Yes',
        'No' => 'No'
    ],

    // choices for appliance-specific fields
    'refrigerator_location' => [
        'kitchen' => 'Kitchen',
        'garage'  => 'Garage',
        'other'   => 'Other'
    ],
    'refrigerator_type' => [
        'side_by_side' => 'Side by side',
        'top_to_bottom' => 'Top-to-bottom',
        'french_door' => 'French door',
        'built_in' => 'Built-in',
    ],
    'dryer_location' => [
        'laundry' => 'Laundry',
        'outside' => 'Outside',
        'garage'  => 'Garage',
        'kitchen'  => 'Kitchen',
        'other'   => 'Other'
    ],
    'dryer_fuel' => [
        'gas'      => 'Natural gas',
        'electric' => 'Electric',
        'propane'     => 'Propane'
    ],
    'dryer_type' => [
        'stackable'      => 'Stackable',
        'all_in_one' => 'All in one',
        'other'     => 'Other'
    ],
    'oven_type' => [
        'single' => 'Single',
        'double' => 'Double',
        'combination' => 'Combination',
    ],
    'washer_location' => [
        'garage'  => 'Garage',
        'laundry' => 'Laundry',
        'closet'  => 'Closet',
        'other'   => 'Other'
    ],
    'washer_load_type' => [
        'top_load'  => 'Top load',
        'front_load' => 'Front load',
    ],
    'washer_type' => [
        'stackable'  => 'Stackable',
        'side_by_side' => 'Side by side',
        'all_in_one' => 'All in one ',
    ],
    'dishwasher_control_location' => [
        'front'  => 'Front',
        'hidden' => 'Hidden',
    ],
    'microwave_type' => [
        'over_the_range'   => 'Over-the-range',
        'built_in'         => 'Built-in',
        'countertop'       => 'Countertop',
        'oven_combination' => 'Oven combination',
    ],
    'range_type' => [
        'freestanding'   => 'Freestanding',
        'slide_in'         => 'Slide-in',
        'drop_in'       => 'Drop-in',
    ],
    'burner_type' => [
        'glass_top'      => 'Glass top',
        'coils'          => 'Coils',
        'sealed_burners' => 'Sealed burners',
    ],
];

    /**
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(InvoiceType::class, $this->get('session')->get('invoice'), ['choices' => $this::choices]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $this->get('session')->set('invoice', $data);
            return $this->redirectToRoute('invoice_sign');
        }

        return [
            'invoice' => $form->createView()
        ];
    }

    /**
     * @Template()
     */
    public function signAction(Request $request)
    {
        if(!$this->get('session')->get('invoice')) {
            return $this->redirectToRoute('invoice_homepage');
        }
        if ($request->request->get('wont_sign', false)) {
            $this->get('session')->set('signature', false);
            return $this->redirectToRoute('invoice_get_pdf');
        } elseif (!empty($request->request->get('signature'))) {
            $this->get('session')->set('signature', $request->request->get('signature'));
            return $this->redirectToRoute('invoice_get_pdf');
        }
        return [
            'invoice' => $this->get('session')->get('invoice'),
            'choices' => $this::choices
        ];
    }

    public function getPdfAction(Request $request)
    {
        if(!$this->get('session')->get('invoice')) {
            return $this->redirectToRoute('invoice_homepage');
        }
        if(!$this->get('session')->get('signature') === null) {
            return $this->redirectToRoute('invoice_sign');
        }

        $all_files = scandir($this->getParameter('kernel.root_dir').'/../web/files/pdf',SCANDIR_SORT_DESCENDING);
        if(count($all_files) > 3) {
            $number = ((int) $all_files[0])+1;
        } else {
            $number = 20000;
        }
        $filePath = $this->getParameter('kernel.root_dir').'/../web/files/pdf/' . $number . '.pdf';
        $invoice = $this->get('session')->get('invoice');

//        return $this->render('@Invoice/PDF/last_appliance.html.twig', [
//            'appliance' => array_shift($invoice['appliances']),
//            'invoice' => $invoice,
//            'choices' => $this::choices,
//            'number'  => $number,
//            'signature' => $this->get('session')->get('signature')
//        ]);

        $pdf_content = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>';
        $i = 0;
        $len = count($invoice['appliances']);
        foreach ($invoice['appliances'] as $key => $appliance) {
//            $items = [];
//            $items[] = [
//                'label' => 'Appliance:',
//                'value' => $appliance['appliance'] === 'other' ? $appliance['appliance_other'] : $this::choices['appliance'][$appliance['appliance']]
//            ];
//            $items[] = [
//                'label' => 'Age (years):',
//                'value' => $appliance['age']
//            ];
//            if(!empty($appliance['size'])) {
//                $items[] = [
//                    'label' => 'Size:',
//                    'value' => $appliance['size'] === 'other' ? $appliance['size_other'] : $appliance['size']
//                ];
//            }
//            if($appliance['appliance'] === 'refrigerator') {
//                $items[] = [
//                    'label' => 'Type:',
//                    'value' => $this::choices['refrigerator_type'][$appliance['refrigerator_type']]
//                ];
//                $items[] = [
//                    'label' => 'Ice maker:',
//                    'value' => $this::choices['yesno'][$appliance['refrigerator_ice_maker']]
//                ];
//                $items[] = [
//                    'label' => 'Water/ice dispenser:',
//                    'value' => $this::choices['yesno'][$appliance['refrigerator_ice_dispenser']]
//                ];
//            }
//            if($appliance['appliance'] === 'washer') {
//                $items[] = [
//                    'label' => 'Location:',
//                    'value' => $appliance['washer_location'] === 'other' ? $appliance['washer_location_other'] : $this::choices['washer_location'][$appliance['washer_location']]
//                ];
//            }
//            if($appliance['appliance'] === 'oven') {
//                $items[] = [
//                    'label' => 'Type:',
//                    'value' => $this::choices['oven_type'][$appliance['oven_type']]
//                ];
//                $items[] = [
//                    'label' => 'Convection:',
//                    'value' => $this::choices['yesno'][$appliance['oven_convection']]
//                ];
//            }
//            if($appliance['appliance'] === 'microwave') {
//                $items[] = [
//                    'label' => 'Type:',
//                    'value' => $this::choices['microwave_type'][$appliance['microwave_type']]
//                ];
//            }
//            if($appliance['appliance'] === 'range') {
//                $items[] = [
//                    'label' => 'Type:',
//                    'value' => $this::choices['range_type'][$appliance['range_type']]
//                ];
//                $items[] = [
//                    'label' => 'Burner type:',
//                    'value' => $this::choices['burner_type'][$appliance['burner_type']]
//                ];
//                $items[] = [
//                    'label' => 'Convection:',
//                    'value' => $this::choices['yesno'][$appliance['range_convection']]
//                ];
//                $items[] = [
//                    'label' => 'Self clean:',
//                    'value' => $this::choices['yesno'][$appliance['range_self_clean']]
//                ];
//            }
//            $items[] = [
//                'label' => 'Brand:',
//                'value' => $appliance['brand'] === 'other' ? $appliance['washer_location_other'] : $this::choices['washer_location'][$appliance['washer_location']]
//            ];




            if ($i == $len - 1) {
                $pdf_content .= $this->renderView('InvoiceBundle:PDF:last_appliance.html.twig', [
                    'appliance' => $appliance,
                    'invoice' => $invoice,
                    'choices' => $this::choices,
                    'number'  => $number,
                    'signature' => $this->get('session')->get('signature')
                ]);
            } else {
                $pdf_content .= $this->renderView('InvoiceBundle:PDF:appliance.html.twig', [
                    'appliance' => $appliance,
                    'number'  => $number,
                    'choices' => $this::choices,
                ]);
            }
            $i++;
        }
        $pdf_content .= '</body></html>';

        $this->get('knp_snappy.pdf')->generateFromHtml($pdf_content, $filePath,
            [
                'margin-top' => 0,
                'margin-right' => 0,
                'margin-bottom' => 0,
                'margin-left' => 0,
                'page-size' => 'A5'
            ]);

        $message = \Swift_Message::newInstance()
            ->setSubject('Your A to Z Appliances invoice #'.$number)
            ->setFrom(['sales@a-zappliances.com'], 'A to Z Appliances')
            ->setTo([$invoice['email']])
            ->attach(\Swift_Attachment::fromPath($filePath))
            ->setBody(
                $this->renderView('InvoiceBundle:Email:invoice.html.twig', [
                    'invoice' => $invoice,
                    'number' => $number
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return $this->redirect('/invoices/thank-you?order_id='.$number);
    }

    /**
     * @Template()
     */
    public function thankAction(Request $request)
    {
        $this->get('session')->clear();
        $invoice_id = $request->query->get('order_id', false);
        return ['slug' => dechex($invoice_id)];
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function viewPdfAction(Request $request, $slug)
    {
        $finder = new Finder();
        $finder->files()->in($this->getParameter('kernel.root_dir').'/../web/files/pdf')->name(hexdec($slug) . '.pdf');
        $iterator = $finder->getIterator();
        $iterator->rewind();
        $file = $iterator->current();
        if(null === $file) {
            throw $this->createNotFoundException('File does not exists');
        }
        return $this->file($file, null, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    public function pdfsAction()
    {
        return $this->redirectToRoute('admin_invoices');
    }

    /**
     * @Template()
     */
    public function newAction() {
        return [];
    }

    public function createAction(Request $request) {
        $requestData = json_decode($request->getContent(), true);
//        var_dump($requestData);
        $number = $requestData['number'];
        $email = $requestData['email'];
        $data = $requestData['data'];

        $invoice = ['first_name' => $requestData['first_name']];

        $filePath = $this->getParameter('kernel.root_dir').'/../web/files/pdf/' . $number . '.pdf';
        if(is_file($filePath)) {
            return $this->json(['response' => 'error', 'message' => 'Invoice #' . $number . ' is already exists.']);
        }
        \file_put_contents($filePath, base64_decode($data));

        $message = \Swift_Message::newInstance()
            ->setSubject('Your A to Z Appliances invoice #'.$number)
            ->setFrom(['sales@a-zappliances.com'], 'A to Z Appliances')
            ->setTo([$email])
            ->attach(\Swift_Attachment::fromPath($filePath))
            ->setBody(
                $this->renderView('InvoiceBundle:Email:invoice.html.twig', [
                    'invoice' => $invoice,
                    'number' => $number
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return $this->json(['response' => 'success']);
    }
}
