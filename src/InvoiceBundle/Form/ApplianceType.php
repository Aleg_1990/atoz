<?php

namespace InvoiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Component\Validator\Constraints\NotBlank;

class ApplianceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $options['choices'];

        $builder
            ->add('appliance', ChoiceType::class, [
                'attr' => [
                    'class' => 'has-feedback'
                ],
                'constraints' => new NotBlank(),
                'choices' => array_flip($choices['appliance']),
                'placeholder' => 'Choose the appliance',
                'choice_attr' => function($val, $key, $index) {
                    return $val === 'other' ? ['style' => 'font-style: italic;'] : [];
                },
            ])
            ->add('appliance_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            // add appliance-specific fields
            ->add('refrigerator_ice_maker', ChoiceType::class, [
                'label' => 'Ice maker',
//                'required' => false,
//                'placeholder' => 'Choose the value',
                'choices' => $choices['yesno'],
                'expanded' => true,
            ])
            ->add('refrigerator_ice_dispenser', ChoiceType::class, [
                'label' => 'Water/ice dispenser',
//                'required' => false,
//                'placeholder' => 'Choose the value',
                'choices' => $choices['yesno'],
                'expanded' => true,
            ])
            ->add('refrigerator_location', ChoiceType::class, [
                'label' => 'Location',
                'required' => false,
                'placeholder' => 'Choose the location',
                'choices' => array_flip($choices['refrigerator_location']),
            ])
            ->add('refrigerator_location_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('refrigerator_type', ChoiceType::class, [
                'label' => 'Type',
                'required' => false,
                'placeholder' => 'Choose the value',
                'choices' => array_flip($choices['refrigerator_type']),
            ])
            ->add('dryer_location', ChoiceType::class, [
                'label' => 'Location',
                'required' => false,
                'placeholder' => 'Choose the location',
                'choices' => array_flip($choices['dryer_location']),
            ])
            ->add('dryer_location_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('dryer_fuel', ChoiceType::class, [
                'label' => 'Fuel',
                'required' => false,
                'placeholder' => 'Choose the fuel',
                'choices' => array_flip($choices['dryer_fuel']),
            ])
            ->add('dryer_type', ChoiceType::class, [
                'label' => 'Type',
                'required' => false,
                'placeholder' => 'Choose the type',
                'choices' => array_flip($choices['dryer_type']),
            ])
            ->add('dryer_type_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('washer_location', ChoiceType::class, [
                'label' => 'Location',
                'required' => false,
                'placeholder' => 'Choose the location',
                'choices' => array_flip($choices['washer_location']),
            ])
            ->add('washer_location_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('washer_load_type', ChoiceType::class, [
                'label' => 'Load type',
                'required' => false,
                'placeholder' => 'Choose the value',
                'choices' => array_flip($choices['washer_load_type']),
            ])
            ->add('washer_type', ChoiceType::class, [
                'label' => 'Type',
                'required' => false,
                'placeholder' => 'Choose the type',
                'choices' => array_flip($choices['washer_type']),
            ])
            ->add('dishwasher_control_location', ChoiceType::class, [
                'label' => 'Control location',
                'required' => false,
                'placeholder' => 'Choose the location',
                'choices' => array_flip($choices['dishwasher_control_location']),
            ])
            ->add('oven_type', ChoiceType::class, [
                'label' => 'Type',
                'placeholder' => 'Choose the value',
                'required' => false,
                'choices' => array_flip($choices['oven_type']),
            ])
            ->add('oven_convection', ChoiceType::class, [
                'label' => 'Convection',
//                'placeholder' => 'Choose the value',
//                'required' => false,
                'choices' => $choices['yesno'],
                'expanded' => true,
            ])
            ->add('microwave_type', ChoiceType::class, [
                'label' => 'Type',
                'required' => false,
                'placeholder' => 'Choose the value',
                'choices' => array_flip($choices['microwave_type']),
            ])
            ->add('range_type', ChoiceType::class, [
                'label' => 'Type',
                'required' => false,
                'placeholder' => 'Choose the value',
                'choices' => array_flip($choices['range_type']),
            ])
            ->add('burner_type', ChoiceType::class, [
                'label' => 'Burner type',
                'required' => false,
                'placeholder' => 'Choose the value',
                'choices' => array_flip($choices['burner_type']),
            ])
            ->add('range_convection', ChoiceType::class, [
                'label' => 'Convection',
//                'required' => false,
//                'placeholder' => 'Choose the value',
                'choices' => $choices['yesno'],
                'expanded' => true,
            ])
            ->add('range_self_clean', ChoiceType::class, [
                'label' => 'Self clean',
//                'required' => false,
//                'placeholder' => 'Choose the value',
                'choices' => $choices['yesno'],
                'expanded' => true,
            ])

            ->add('brand', ChoiceType::class, [
                'constraints' => new NotBlank(),
                'choices' => array_flip($choices['brand']),
                'placeholder' => 'Choose the brand',
                'choice_attr' => function($val, $key, $index) {
                    return $val === 'other' ? ['style' => 'font-style: italic;'] : [];
                },
            ])
            ->add('brand_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('model', TextType::class, [
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the model',
                ]
            ])
            ->add('serial_number', TextType::class, [
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type serial number',
                ]
            ])
            ->add('customer_complaint', TextareaType::class, [
                'constraints' => new NotBlank(),
            ])
            ->add('tech_recommendation', TextareaType::class, [
                'constraints' => new NotBlank(),
            ])
            ->add('age', IntegerType::class, [
                'label' => 'Age in years',
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('color', ChoiceType::class, [
                'constraints' => new NotBlank(),
                'choices' => array_flip($choices['color']),
                'placeholder' => 'Choose the color',
                'choice_attr' => function($val, $key, $index) {
//                var_dump($val);
                    $styles = [
                        'stainless' => 'background-color: #e0dfdb',
                        'black' => 'background-color: #000; color: #fff',
                        'red' => 'background-color: #f00; color: #fff',
                        'green' => 'background-color: #008000; color: #fff',
                        'blue' => 'background-color: #00f; color: #fff',
                        'grey' => 'background-color: #808080; color: #fff',
                        'wood_panels' => 'background-color: #966F33; color: #fff',
                        'other' => 'font-style: italic',
                    ];
                    if(isset($styles[$val])) {
                        return ['style' => $styles[$val]];
                    }
                    return [];
                },
            ])
            ->add('color_other', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the color',
                ]
            ])
            ->add('condition', ChoiceType::class, [
                'constraints' => new NotBlank(),
//                'required' => false,
                'choices' => array_flip($choices['condition']),
                'placeholder' => 'Choose condition',
            ])
            ->add('size', ChoiceType::class, [
//                'constraints' => new NotBlank(),
                'required' => false,
                'choices' => [
                    '18' => '18',
                    '24' => '24',
                    '27' => '27',
                    '30' => '30',
                    '36' => '36',
                    '48' => '48',
                    '64' => '64',
                    'Other' => 'other'
                ],
                'placeholder' => 'Choose size',
                'choice_attr' => function($val, $key, $index) {
                    return $val === 'other' ? ['style' => 'font-style: italic;'] : [];
                },
            ])
            ->add('size_other', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the size',
                ]
            ])
            ->add('fuel', ChoiceType::class, [
                'required' => false,
//                'constraints' => new NotBlank(),
                'choices' => array_flip($choices['fuel']),
                'placeholder' => 'Choose the type',
            ])
            ->add('burners', IntegerType::class, [
                'required' => false,
//                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('cycles', IntegerType::class, [
                'required' => false,
//                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('options', IntegerType::class, [
                'required' => false,
//                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
        ;
    }

    /**
     * @{inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => true,
                'data-toggle' => 'validator'
            ]
        ])
        ->setDefined('choices')
        ;
    }
    /**
     * @{inheritdoc}
     */
    public function getName()
    {
        return 'appliance';
    }
}