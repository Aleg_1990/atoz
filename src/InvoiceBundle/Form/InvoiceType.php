<?php

namespace InvoiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $options['choices'];
        $builder
            ->add('appliances', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'entry_type' => ApplianceType::class,
                'entry_options'  => [
                    'label' => false,
                    'choices' => $options['choices'],
                ]
            ])

            ->add('address', TextType::class, [
                'label' => 'Customer address',
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the address',
                ]
            ])
            ->add('region', ChoiceType::class, [
                'constraints' => new NotBlank(),
                'choices' => array_flip($choices['region']),
                'expanded' => true,
            ])
            ->add('amount_due', ChoiceType::class, [
                'constraints' => new NotBlank(),
                'choices' => [
                    '$0' => '$0',
                    '$25.00' => '$25.00',
                    '$60.00' => '$60.00',
                    '$65.00' => '$65.00',
                    '$75.00' => '$75.00',
                    '$100.00' => '$100.00',
                    '$125.00' => '$125.00',
                    'Other' => 'other',
                ],
                'placeholder' => 'Choose the amount',
                'choice_attr' => function($val, $key, $index) {
                    return $val === 'other' ? ['style' => 'font-style: italic;'] : [];
                },
            ])
            ->add('amount_due_other', MoneyType::class, [
                'label' => false,
                'required' => false,
                'currency' => 'USD',
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('payment_method', ChoiceType::class, [
                'label' => 'Payment',
                'choices' => array_flip($choices['payment_method']),
                'placeholder' => 'Choose the value',
            ])
            ->add('check_number', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Type the check number',
                ]
            ])
            ->add('add_estimate', CheckboxType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('estimate', MoneyType::class, [
                'required' => false,
                'currency' => 'USD',
                'attr' => [
                    'placeholder' => 'Type the value',
                ]
            ])
            ->add('first_name', TextType::class, [
                'label' => 'Customer first name',
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the first name',
                ]
            ])
            ->add('last_name', TextType::class, [
                'label' => 'Customer last name',
                'constraints' => new NotBlank(),
                'attr' => [
                    'placeholder' => 'Type the last name',
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Customer email',
                'constraints' => [new NotBlank(), new Email()],
                'attr' => [
                    'placeholder' => 'Type the email',
                ]
            ])
        ;
    }

    /**
     * @{inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => true,
                'data-toggle' => 'validator'
            ]
        ])
        ->setDefined('choices')
        ;
    }
}