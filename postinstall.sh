#!/bin/bash
bin/console doctrine:schema:create -e dev
bin/console fos:user:create --super-admin sales@a-zappliances.com sales@a-zappliances.com atozpassword
bin/console doctrine:query:sql "Update fos_user set firstname='Admin' where username='admin'"
