'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
    'ngAnimate',
  'ui.mask',
    'ui.router',
    'angular-ladda',
  'myApp.home',
  'myApp.book',
  'myApp.contact',
  'myApp.thank',
  'myApp.corporate'
]).config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $routeProvider.otherwise({
      templateUrl: 'components/error/error.html',
  });
    $locationProvider.html5Mode(true);
}]).run(function ($rootScope, $http, $location) {

    $rootScope.contact = {
        submit: function(formName) {
            $rootScope.sending = true;
          $http
              .post('contact-submit', angular.extend($rootScope.contact, {'type': formName}))
              .then(function() {
                  $rootScope.sending = false;
                  $location.path('thank');
          })
        }
    }
});
