'use strict';

angular.module('myApp.book', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/book', {
    templateUrl: 'components/book/book.html',
    controller: 'BookCtrl'
  });
}])

.controller('BookCtrl', [function() {

}]);