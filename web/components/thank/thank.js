'use strict';

angular.module('myApp.thank', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/thank', {
    templateUrl: 'components/thank/thank.html',
    controller: 'ThankCtrl'
  });
}])

.controller('ThankCtrl', [function() {

}]);