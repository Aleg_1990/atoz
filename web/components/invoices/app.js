'use strict';

angular.module('invoiceApp', [
    // 'ngRoute',
    // 'ngAnimate',
    // 'ui.mask',
    // 'ui.router',
    // 'angular-ladda',
    'invoiceApp.home'
]);

angular.module('invoiceApp.home', [])
    .controller('MainCtrl', ['$scope', '$http', function($scope, $http) {
        $scope.$root.step = {
            index: 1,
            prev: function() {
                if($scope.$root.step.index !== 1) {
                    $scope.$root.step.index--;
                }
            },
            next: function() {
                // if($scope.$root.step.index < $scope.$root.step.max) {
                    $scope.$root.step.index++;
                // }
            }
        };

        $scope.refreshNumber = function() {
            $scope.number = Math.floor(Math.random() * (1000000 - 10000)) + 10000;
        };
        $scope.refreshNumber();

        var createPdf = function() {
            var invoice = $scope.invoice;
            var appliances = [];


            angular.forEach($scope.invoice.appliances, function(appliance, i) {
                var normalizedAppliance = [];
                var cnt = 0;
                var addItem = function(label, value) {
                    if (cnt === 0) {
                        normalizedAppliance.push([]);// add row
                    }
                    normalizedAppliance[normalizedAppliance.length-1].push({
                        text: label,
                        style: {
                            alignment: 'right',
                            bold:true
                        }
                    });
                    normalizedAppliance[normalizedAppliance.length-1].push({
                        text: value
                    });
                    cnt++;
                    if(cnt === 3) {
                        cnt = 0;
                    }
                };
                addItem('Appliance:', appliance.appliance == 'other' ?  appliance.appliance_other: appliance.appliance);
                addItem('Brand:', appliance.brand == 'other' ?  appliance.brand_other: appliance.brand);
                addItem('Model:', appliance.model);
                addItem('Serial number:', appliance.serial_number);
                if (appliance.appliance == 'Refrigerator') {
                    addItem('Ice maker:', appliance.refrigerator_ice_maker);
                    addItem('Water/ice dispenser:', appliance.refrigerator_ice_dispenser);
                    addItem('Type:', appliance.refrigerator_type);
                }
                if (appliance.appliance == 'Dryer') {
                    addItem('Location:', appliance.dryer_location);
                    addItem('Fuel:', appliance.dryer_fuel);
                    addItem('Type:', appliance.dryer_type == 'other' ?  appliance.dryer_type_other : appliance.dryer_type);
                }

                if (appliance.appliance == 'Washer') {
                    addItem('Location:', appliance.washer_location == 'other' ?  appliance.washer_location_other : appliance.washer_location);
                    addItem('Load type:', appliance.washer_load_type);
                    addItem('Type:', appliance.washer_type)
                }

                if (appliance.appliance == 'Dishwasher') {
                    addItem('Control location:', appliance.dishwasher_control_location)
                }

                if (appliance.appliance == 'Oven') {
                    addItem('Type:', appliance.oven_type);
                    addItem('Convection:', appliance.oven_convection)
                }

                if (appliance.appliance == 'Microwave') {
                    addItem('Type:', appliance.microwave_type)
                }

                if (appliance.appliance == 'Range') {
                    addItem('Type:', appliance.range_type);
                    addItem('Burner type:', appliance.burner_type);
                    addItem('Convection:', appliance.range_convection);
                    addItem('Self clean:', appliance.range_self_clean)
                }

                addItem('Age in years:', appliance.age);
                addItem('Color:', appliance.color == 'other' ?  appliance.color_other: appliance.color);
                addItem('Condition:', appliance.condition);
                if (appliance.size) {
                    addItem('Size:', appliance.size+'"');
                }
                if (appliance.fuel) {
                    addItem('Fuel:', appliance.fuel);
                }
                if (appliance.burners) {
                    addItem('Burners:', appliance.burners);
                }
                if (appliance.cycles) {
                    addItem('Cycles:', appliance.cycles);
                }
                if (appliance.options) {
                    addItem('Options:', appliance.options);
                }

                for(var j=normalizedAppliance[normalizedAppliance.length-1].length; j<=5; j++) {
                    normalizedAppliance[normalizedAppliance.length-1].push({
                        text: ''
                    });
                }


                var summary = [
                    [
                        {
                            text: 'Tech recommendation: '+ appliance.tech_recommendation,
                            style: {
                                italic: true
                            }
                        }
                    ]
                ];

                invoice = $scope.invoice;
                if(invoice.appliances.length === i + 1) {
                    summary[0].push({
                        text: 'Total:',
                        style: {
                            alignment: 'right',
                            bold: true
                        }
                    });
                    summary[0].push({
                        text: '$'+parseFloat(invoice.amount_due).toFixed(2)
                    });
                    if(invoice.amount_due !== '0') {
                        summary.push([
                            {
                                text: ''
                            },
                            {
                                text: 'Payment:',
                                style: {
                                    alignment: 'right',
                                    bold: true
                                }
                            },
                            {
                                text: (invoice.payment_method === 'Check') ? invoice.payment_method+', '+invoice.check_number : invoice.payment_method
                            }
                        ])
                    }
                    if(invoice.estimate) {
                        summary.push([
                            {
                                text: ''
                            },
                            {
                                text: 'Estimate:',
                                style: {
                                    alignment: 'right',
                                    bold: true
                                }
                            },
                            {
                                text: '$'+parseFloat(invoice.estimate).toFixed(2)
                            }
                        ])
                    }
                }



                var applianceObj = [
                    {
                        stack: [
                            {
                                text: 'Appliance details',
                                style: {
                                    fontSize: 14,
                                    bold: true
                                }
                            },
                            {
                                canvas: [
                                    {
                                        type: 'line',
                                        x1: 0, y1: 10,
                                        x2: 200, y2: 10,
                                        lineWidth: 1,
                                        lineColor: 'black'
                                    }
                                ]
                            },
                            {
                                table: {
                                    widths: [ '*','*', '*', '*', '*', '*'],
                                    body: normalizedAppliance

                                },
                                layout: 'noBorders',
                                marginTop: 20,
                                style: {
                                    fontSize: 10
                                }

                            }
                        ],
                        style: 'pdfSection'
                    },
                    {
                        stack: [
                            {
                                text: 'Work order details',
                                style: {
                                    fontSize: 14,
                                    bold: true
                                }
                            },
                            {
                                canvas: [
                                    {
                                        type: 'line',
                                        x1: 0, y1: 10,
                                        x2: 200, y2: 10,
                                        lineWidth: 1,
                                        lineColor: 'black'
                                    }
                                ]
                            },
                            {
                                text: 'Customer complaint',
                                style: {
                                    fontSize: 10,
                                    bold: true
                                },
                                marginTop: 20
                            },
                            {
                                text: appliance.customer_complaint,
                                style: {
                                    fontSize: 10
                                },
                                marginTop: 20
                            },
                            {
                                canvas: [
                                    {
                                        type: 'line',
                                        x1: 0, y1: 10,
                                        x2: 530, y2: 10,
                                        lineWidth: 0.2,
                                        lineColor: 'black'
                                    }
                                ]
                            },
                            {
                                table: {
                                    widths: [ '*','15%', '15%'],
                                    body: summary

                                },
                                layout: 'noBorders',
                                marginTop: 20,
                                style: {
                                    fontSize: 10
                                }

                            }
                        ],
                        style: 'pdfSection'
                    }
                ];

                if (invoice.appliances.length !== i + 1) {
                    applianceObj[1].pageBreak = 'after';
                }

                appliances.push(applianceObj)

            });


            var today = new Date();
            var dd = today.getDate();

            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10)
            {
                dd='0'+dd;
            }

            if(mm<10)
            {
                mm='0'+mm;
            }

            const docDefinition = {
                pageSize: 'A4',
                pageMargins: [ 0, 140, 0, 20 ],
                header: {
                    table: {
                        widths: [ '47%','53%'],
                        body: [
                            [
                                {
                                    stack: [
                                        {
                                            text: "A to Z Appliances\n",
                                            style:
                                                {
                                                    fontSize: 15,
                                                    bold: true
                                                }

                                        },
                                        {
                                            text: "PO BOX 500202, San Diego, CA 92150\n License number: A47448",
                                            style:
                                                {
                                                    color: '#959697'
                                                    // fontSize: 8,
                                                },
                                            marginTop: 10
                                        },
                                        {
                                            text: $scope.number,
                                            style:
                                                {
                                                    fontSize: 23,
                                                    color: '#2c3393'

                                                },
                                            marginTop: 10
                                        },
                                        {
                                            text: "Issued: "+mm+'/'+dd+'/'+yyyy,
                                            style:
                                                {
                                                    color: '#959697'
                                                    // fontSize: 8,
                                                },
                                            marginTop: 10
                                        }
                                    ],
                                    margin: [15, 15],
                                    fillColor: '#fafafa'
                                },
                                {
                                    stack: [
                                        {
                                            text: [
                                                "Ask us ",
                                                {
                                                    text: 'anything',
                                                    style:
                                                        {
                                                            decoration: 'underline'
                                                        }
                                                },
                                                " online!"
                                            ],
                                            style:
                                                {
                                                    fontSize: 12,
                                                    bold: true,
                                                    alignment: 'center',
                                                    color: '#FFFFFF'
                                                }

                                        },
                                        {
                                            text: "a-zappliances.com",
                                            link: "https://a-zappliances.com",
                                            style:
                                                {
                                                    color: '#FBC412',
                                                    fontSize: 24,
                                                    bold: true,
                                                    alignment: 'center'
                                                },
                                            marginTop: 15
                                        },
                                        {
                                            text: "Customer support chat",
                                            style:
                                                {
                                                    color: '#ffffff',
                                                    bold: true,
                                                    alignment: 'center',
                                                    fontSize: 9
                                                },
                                            marginTop: 20
                                        },
                                        {
                                            text: "THE FASTEST WAY TO GET ASSISSTED",
                                            style:
                                                {
                                                    color: '#9397c7',
                                                    bold: true,
                                                    alignment: 'center',
                                                    fontSize: 5
                                                },
                                            marginTop: 10
                                        }
                                    ],
                                    margin: [0, 20],
                                    fillColor: '#2C3393'
                                }
                            ]
                        ]
                    },
                    layout: 'noBorders'
                },
                footer: {
                    table: {
                        widths: [ '100%'],
                        body: [
                            [
                                {
                                    text: "We are not responsible for water damage, loss of food, or damage to floors, walls and cabinets\n800-567-8602   |   760-260-5003",
                                    style: {
                                        alignment: 'center'
                                    },
                                    fontSize: 6,
                                    color: '#959697',
                                    fillColor: '#fafafa'
                                }
                            ]
                        ]
                    },
                    layout: 'noBorders'
                },

                content: [
                    {
                        stack: appliances
                    },


                    {
                        stack: [
                            {
                                text: 'Payment details',
                                style: {
                                    fontSize: 14,
                                    bold: true
                                }
                            },
                            {
                                canvas: [
                                    {
                                        type: 'line',
                                        x1: 0, y1: 10,
                                        x2: 200, y2: 10,
                                        lineWidth: 1,
                                        lineColor: 'black'
                                    }
                                ]
                            },
                            {
                                table: {
                                    widths: ['15%', '*'],
                                    body: [
                                        [
                                            {
                                                text: 'Client:',
                                                style: {
                                                    alignment: 'right',
                                                    bold: true
                                                }
                                            },
                                            {
                                                text: invoice.first_name+' '+invoice.last_name
                                            }
                                        ],
                                        [
                                            {
                                                text: 'Address:',
                                                style: {
                                                    alignment: 'right',
                                                    bold: true
                                                }
                                            },
                                            {
                                                text: invoice.address+' '+invoice.region
                                            }
                                        ],
                                        [
                                            {
                                                text: 'Email:',
                                                style: {
                                                    alignment: 'right',
                                                    bold: true
                                                }
                                            },
                                            {
                                                text: invoice.email
                                            }
                                        ]
                                    ]
                                },
                                layout: 'noBorders',
                                marginTop: 20,
                                style: {
                                    fontSize: 10
                                }

                            }
                        ],
                        style: 'pdfSection'
                    }

                ],
                styles: {
                    pdfSection: {
                        margin: [31, 33, 31, 0]
                    }
                },
                defaultStyle: {
                    fontSize: 8
                }
            };

            if(invoice.signature) {
                docDefinition.content.push({
                    stack: [
                        {
                            image: invoice.signature,
                            width: 200,
                            marginRight: 10,
                            style: {
                                alignment: 'right'
                            }
                        },
                        {
                            canvas: [
                                {
                                    type: 'line',
                                    x1: 330, y1: 0,
                                    x2: 530, y2: 0,
                                    lineWidth: 1,
                                    lineColor: 'black'
                                }
                            ]
                        },
                        {
                            text: 'customer signature',
                            width: 200,
                            marginTop: 3,
                            marginRight: 60,
                            style: {
                                alignment: 'right',
                                fontSize: 10
                            }

                        }
                    ],
                    style: 'pdfSection'
                });
            }

            return pdfMake.createPdf(docDefinition)
        };

        $scope.getPdf = function() {
            createPdf().getDataUrl(function(dataUrl) {
                window.open(dataUrl, '_blank');
            })


        };

        $scope.sendPdf = function() {
            createPdf().getBase64(function(data) {
                $scope.sending = true;
                $http.post('/invoices/create', {data: data, number: $scope.number, email: $scope.invoice.email, first_name: $scope.invoice.first_name}).then(
                    function(response) {
                        if(angular.isObject(response.data) && response.data.response === 'error') {
                            $scope.sendMessage = response.data.message;
                        }
                        if(angular.isObject(response.data) && response.data.response === 'success') {
                            $scope.sent = true;
                            $scope.sendMessage = false;
                        }
                        $scope.sending = false;
                    }, function() {
                        $scope.sending = false;
                        $scope.sendMessage = 'Unknown error, probably network.';
                    }
                )
            });
        };

    }])
    .controller('FormCtrl', ['$scope', function($scope) {
        $scope.invoiceFormSubmitted = false;
        $scope.scrollToError = function() {
            // var anchor = $scope.invoiceForm.$error.required[0].$name;
            // console.log(anchor);
            $('select.ng-invalid, input.ng-invalid').eq(0).focus();
            // $location.hash('invoice_address'.anchor);
            // $anchorScroll('invoice_'+anchor);
        };
        $scope.invoice = {
            appliances: [{}], // one appliance by default
            removeAppliance: function(appliance) {
                var index = $scope.invoice.appliances.indexOf(appliance);
                $scope.invoice.appliances.splice(index, 1);
            },
            submit: function() {
                $scope.$root.step.next();
                $scope.$root.invoice = $scope.invoice;
            }
        };
    }])
    // @see http://stackoverflow.com/questions/16388562/angularjs-force-uppercase-in-textbox
    .directive('capitalize', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {
                var capitalize = function(inputValue) {
                    if (inputValue == undefined) inputValue = '';
                    var capitalized = inputValue.toUpperCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                };
                modelCtrl.$parsers.push(capitalize);
                capitalize(scope[attrs.ngModel]); // capitalize initial value
            }
        };
    })
    // @see https://gist.github.com/escapedcat/6be8ab86c97be42b6882
    .directive('jSignatureDirective', function() {
    return {
        restrict: 'E',
        link: function($scope, $element) {

            console.log('jSignatureDirective: link');
            console.dir($scope);

            /*
             width   Defines the width of the canvas. Numerical value without % or px    250
             height  Defines the height of the canvas. Numerical value without % or px   150
             color   Defines the color of the strokes on the canvas. Accepts any color hex values.   #000
             background-color    Defines the background color of the canvas. Accepts any color hex values.   #fff
             lineWidth   Defines the thickness of the lines. Accepts any positive numerical value    1
             cssclass    Defines a custom class for the canvas.  None
             */

            $scope.initialized = false;

            $scope.initialize = function () {
                if(!$scope.initialized) {
                    $element
                        .addClass('form-input')
                        .jSignature({
                            sizeRatio: 2
                        });
                    $element
                        .on('change', function() {
                            $scope.invoice.signature = $element.jSignature("getData");
                        })
                        .on('mousedown', function() {
                            $('#wont-sign').prop('checked', false)
                        });


                    $scope.initialized = true;
                }
            };
            $('#wont-sign').on('change', function() {
//                                        if($('#wont-sign').is(':checked')) {

                $element.jSignature('clear');
                $scope.invoice.signature = false;
//                                        }
            });

            $("#signature-clear").on('click', function () {
                $element.jSignature('clear')
            });


            $scope.initialize();
        }
    };
});


if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function(reg) {
        // регистрация сработала
        console.log('Registration succeeded. Scope is ' + reg.scope);
    }).catch(function(error) {
        // регистрация прошла неудачно
        console.log('Registration failed with ' + error);
    });
}