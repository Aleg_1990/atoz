'use strict';

angular.module('myApp.corporate', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/corporate', {
    templateUrl: 'components/corporate/corporate.html',
    controller: 'CorporateCtrl'
  });
}])

.controller('CorporateCtrl', [function() {

}]);