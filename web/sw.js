const CACHE_VERSION = 7;
self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open('static-v'+CACHE_VERSION).then(function(cache) {
            return cache.addAll([
                '/invoicing',
                '/network_error.json',
                '/bower_components/bootstrap/dist/css/bootstrap.min.css',
                '/css/main.css',
                '/bower_components/jquery/dist/jquery.min.js',
                '/bower_components/jSignature/libs/modernizr.js',
                '/bower_components/angular/angular.min.js',
                '/bower_components/jSignature/libs/jSignature.min.js',
                '/bower_components/pdfmake/build/pdfmake.min.js',
                '/bower_components/ladda/dist/spin.min.js',
                '/components/invoices/app.js',
                '/components/invoices/vfs_fonts.js',
                '/logo_atoz.png',
                '/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff',
                '/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf',
                '/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2'
            ]);
        })
    );
});

self.addEventListener('activate', function(event) {
    var cacheWhitelist = ['static-v'+CACHE_VERSION];

    event.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if (cacheWhitelist.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }));
        })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches
            .match(event.request)
            .then(function(response) {
                return response || fetch(event.request);
            })
            .catch(function() {
                if('POST' === event.request.method/* || 'PUT' === event.request.method*/) {
                    return caches.match('/network_error.json');
                }
            })
    );
});